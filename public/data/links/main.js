const data = {
	"Common": {
		"VimWiki": "file:///home/manuel/src/vimwiki/default/public/index.html",
		"CUPS config": "http://192.168.0.35:631",
		"DuckDuckGo": "https://www.duckduckgo.com",
		"Tutanota": "https://tutanota.com",
		"Gmail": "https://mail.google.com",
		"Drive": "https://drive.google.com/drive/my-drive"
	},
	"University": {
		"Learn homepage": "https://www.learn.ed.ac.uk/webapps/portal/execute/tabs/tabAction?tab_tab_group_id=_61_1",
		"MyEd": "https://www.myed.ed.ac.uk/",
		"BetterInformatics": "https://betterinformatics.com/",
		"Outlook": "https://outlook.office365.com",
		"Piazza": "https://piazza.com",
		"EveryonePrint": "https://www.everyoneprint.is.ed.ac.uk",
		"SeatEd": "https://seated.iot.ed.ac.uk",
		"MyPrint": "https://myprint.is.ed.ac.uk/user"
	},
	"Programming": {
		"GitLab": "https://gitlab.com/",
		"GitHub": "https://github.com/",
		"HackerRank": "http://www.hackerrank.com/",
		"Stack Overflow": "http://stackoverflow.com/",
		"Tutorials": "tutorials.html"
	},
	"Misc": {
		"Steam": "http://store.steampowered.com/",
		"GOG": "https://www.gog.com/",
		"Media Index": "media-index/media-index.html",
		"Inventory": "inventory.html",
		"Tortuga": "tortuga.html"
	},
	"Linux": {
		"OpenMediaVault": "https://192.168.0.14",
		"Void Linux": "https://voidlinux.org/",
		"ArchWiki": "https://wiki.archlinux.org/"
	}
}
