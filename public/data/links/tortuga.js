const data = {
	"Pirate Stuff": {
		"The Pirate Bay": "https://thepiratebay.org",
		"1337x": "https://1337x.to",
		"Torrentz2": "https://torrentz2.eu",
		"Library Genesis": "http://gen.lib.rus.ec",
		"B-OK": "https://b-ok.org",
		"RARBG": "https://rarbg.to"
	}
}
