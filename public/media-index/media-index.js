"use strict";

function getDateString(epoch) {
	let utcSeconds = epoch;
	let d = new Date(0); // The 0 there is the key, which sets the date to the epoch
	d.setUTCSeconds(utcSeconds);
	return d.toLocaleString();
}

function fixTitle(str) {
	return str
		.replace(/_/g, " ")
		.replace(/ [a-z]/g, match => ` ${match[1].toUpperCase()}`)
		.replace(/^[a-z]/, match => match[0].toUpperCase());
}

function createMovieDOMElement(entry) {
	let entryElem = document.createElement("tr");
	let entryName = document.createElement("td");
	let entryNameLink = document.createElement("a");
	let entryDisk = document.createElement("td");
	let entryDate = document.createElement("td");

	entryNameLink.href = `./movie_pages/${entry["filename"]}`;
	entryNameLink.textContent = fixTitle(entry["filename"]);
	entryDisk.textContent = entry["disk_label"];
	entryDate.textContent = getDateString(entry["date_created"]);

	entryName.appendChild(entryNameLink);
	entryElem.appendChild(entryName);
	entryElem.appendChild(entryDisk);
	entryElem.appendChild(entryDate);

	entryElem.className = "movieEntry";

	return entryElem;
}

function createMovieTableHeader() {
	// Create table header row
	let tableHeader = document.createElement("tr");
	let tableHeaderName = document.createElement("th");
	let tableHeaderDisk = document.createElement("th");
	let tableHeaderDate = document.createElement("th");

	tableHeaderName.textContent = "Name";
	tableHeaderDisk.textContent = "Disk";
	tableHeaderDate.textContent = "Date added";

	tableHeader.appendChild(tableHeaderName);
	tableHeader.appendChild(tableHeaderDisk);
	tableHeader.appendChild(tableHeaderDate);

	tableHeader.className = "movieTableHeader";

	return tableHeader;
}

let sortFuncs = {
	date: (a, b) => a["date_created"] - b["date_created"],
	name: (a, b) => a["filename"].localeCompare(b["filename"]),
	diskId: (a, b) =>
		`${a["disk_label"]}${a["filename"]}`.localeCompare(
			`${b["disk_label"]}${b["filename"]}`
		)
};

function performSort() {
	let disk_usage_element = document.querySelector("#disk_usage");
	disk_usage_element.innerHTML = "";
	disk_usage.map(disk_data => {
		let curr_elem = document.createElement("div");
		let curr_elem_meter = document.createElement("meter");
		let curr_elem_label = document.createElement("label");
		curr_elem_label.textContent = `${disk_data["disk_label"]} ${
			disk_data["used"]
		}G/${disk_data["used"] + disk_data["avail"]}G `;
		curr_elem_meter.id = disk_data["disk_label"];
		curr_elem_meter.value = disk_data["used"];
		curr_elem_meter.min = 0;
		curr_elem_meter.max = disk_data["used"] + disk_data["avail"];
		curr_elem.appendChild(curr_elem_label);
		curr_elem.appendChild(curr_elem_meter);
		disk_usage_element.appendChild(curr_elem);
		disk_usage_element.appendChild(document.createElement("br"));
	});
	let movieList = document.querySelector("#movies_list");
	movieList.innerHTML = "";
	movieList.appendChild(createMovieTableHeader());
	let chosenRadioButton = document.querySelector(
		'input[name="sortCriteria"]:checked'
	);
	let sortCriteria =
		chosenRadioButton == null ? "name" : chosenRadioButton.value;

	data
		.sort(sortFuncs[sortCriteria])
		.map(createMovieDOMElement)
		.map(elem => movieList.appendChild(elem));
}

performSort();
