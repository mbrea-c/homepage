"use strict";

function getDateString(epoch) {
	let utcSeconds = epoch;
	let d = new Date(0); // The 0 there is the key, which sets the date to the epoch
	d.setUTCSeconds(utcSeconds);
	return d.toLocaleString();
}

function fixTitle(str) {
	return str
		.replace(/_/g, " ")
		.replace(/ [a-z]/g, match => ` ${match[1].toUpperCase()}`)
		.replace(/^[a-z]/, match => match[0].toUpperCase());
}

function getChildrenDOMList(entry) {
	let listElem = document.createElement("li");
	listElem.textContent = fixTitle(entry["filename"]);
	let childrenList = document.createElement("ul");
	let children = entry["children"];
	children.map(child => childrenList.appendChild(getChildrenDOMList(child)));
	if (children.length > 0) {
		listElem.appendChild(childrenList);
	}
	return listElem;
}

let header = document.querySelector(".title");
let matchedEntry = data.filter(entry => entry["filename"] == header.id)[0];
let detailsElem = document.querySelector("#details");

// Update title
let title = fixTitle(matchedEntry["filename"]);
header.textContent = title;

// Add date details
let dateAdded = document.createElement("p");
dateAdded.textContent = `Date added: ${getDateString(
	matchedEntry["date_created"]
)}`;
let diskId = document.createElement("p");
diskId.textContent = `Stored in: ${matchedEntry["disk_label"]}`;

detailsElem.appendChild(dateAdded);
detailsElem.appendChild(diskId);

if (matchedEntry["children"].length > 0) {
	console.log(matchedEntry["children"]);
	let contentsHeader = document.createElement("h2");
	contentsHeader.textContent = "Contents";
	let parentList = document.createElement("ul");
	parentList.appendChild(getChildrenDOMList(matchedEntry));
	detailsElem.appendChild(contentsHeader);
	detailsElem.appendChild(parentList);
}

console.log(matchedEntry);
