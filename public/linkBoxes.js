const root = document.body

console.log("running script")

Object.entries(data).map(([section_name, links]) => {
	const container = document.createElement("div")
	container.className = "all"
	const title_container = document.createElement("div")
	title_container.className = "title linkheader"
	const title = document.createElement("h3")
	title.textContent = section_name
	const links_container = document.createElement("div")
	links_container.className = "links"

	Object.entries(links).map(([ text, link ]) => {
		const link_element = document.createElement("a")
		link_element.href = link
		const link_text = document.createElement("p")
		link_text.textContent = text

		link_element.appendChild(link_text)
		links_container.appendChild(link_element)
	})

	title_container.appendChild(title)
	container.appendChild(title_container)
	container.appendChild(links_container)
	root.appendChild(container)
});
